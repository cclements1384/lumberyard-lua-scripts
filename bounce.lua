local bounce = 
{
	Properties = 
	{
		InitialVelocity = {default=Vector3(0,0,0)},
		Impulse = {default=Vector3(0,0,0)}
	}
}

function bounce:OnActivate()

	self.tickBus = TickBus.Connect(self, 0)
	--PhysicsComponentRequestBus.Event.SetVelocity(self.entityId, self.Properties.InitialVelocity)

end

function bounce:OnTick(dt)

	PhysicsComponentRequestBus.Event.AddImpulse(self.entityId, self.Properties.Impulse)

end

function bounce:OnDeactivate()

	self.tickBus:Disconnect()

end

return bounce