local controller = 
{
	Properties = 
	{
		mouseX = "",
		mouseY = "",
		camSpeed = 0.0,
		playerSpeed = .03,
	    mainCamera = { default = EntityId()}
	} 
}

function controller:OnActivate()

	----------------------------====   keyboard input Id and bus connection  ====--------------------------------
	self.forwardId = InputEventNotificationId("Forward")
	self.forwardBus = InputEventNotificationBus.Connect(self, self.forwardId)
	
	self.backwardId = InputEventNotificationId("Backward")
	self.backwardBus = InputEventNotificationBus.Connect(self, self.backwardId)

	self.leftId = InputEventNotificationId("Left")
	self.leftBus = InputEventNotificationBus.Connect(self, self.leftId)
	
	self.rightId = InputEventNotificationId("Right")
	self.rightBus = InputEventNotificationBus.Connect(self, self.rightId)
	
	------------------------------====   mouse input Id and bus connection  ====----------------------------------
	self.mouseXId = InputEventNotificationId(self.Properties.mouseX)
	self.mouseXBus = InputEventNotificationBus.Connect(self, self.mouseXId)
	
	self.mouseYId = InputEventNotificationId(self.Properties.mouseY)
	self.mouseYBus = InputEventNotificationBus.Connect(self, self.mouseYId)
	
end

function controller:OnHeld(InputValue)
	
	-------------------------------------====   local variables  ====-----------------------------------------
	local camSpeed = self.Properties.camSpeed
	local mainCamera = self.Properties.mainCamera
	local playerSpeed = self.Properties.playerSpeed
	local playerTransformMatrix = TransformBus.Event.GetWorldTM(self.entityId)
	local forwardVector = playerTransformMatrix:GetColumn(1)   -- gets the forward vector of a given tranform.
	local rightVector = playerTransformMatrix:GetColumn(0)

	-------------------------------------====   keyboard  events ====-----------------------------------------
	if(InputEventNotificationBus.GetCurrentBusId() == self.forwardId) then
		-- move the entity, while holding the forward button by the movement speed
		TransformBus.Event.MoveEntity(self.entityId, forwardVector * InputValue *  playerSpeed)
	end
	
	if(InputEventNotificationBus.GetCurrentBusId() == self.backwardId) then
		-- move the entity, while holding the backward button by the movement speed
		TransformBus.Event.MoveEntity(self.entityId, -forwardVector * InputValue * playerSpeed)
	end
	
	if(InputEventNotificationBus.GetCurrentBusId() == self.leftId) then
		-- move the entity, while holding the left button by the movement speed
			TransformBus.Event.MoveEntity(self.entityId, -rightVector * InputValue *  playerSpeed)
	end
	
	if(InputEventNotificationBus.GetCurrentBusId() == self.rightId) then
		-- move the entity, while holding the right button by the movement speed
			TransformBus.Event.MoveEntity(self.entityId, rightVector * InputValue *  playerSpeed)
	end
	
	-------------------------------------====   mouse events ====-----------------------------------------
	if(InputEventNotificationBus.GetCurrentBusId() == self.mouseXId) then
		TransformBus.Event.RotateByZ(self.entityId, - InputValue * camSpeed)
	end
	
	if(InputEventNotificationBus.GetCurrentBusId() == self.mouseYId) then
		TransformBus.Event.RotateByX(mainCamera, - InputValue * camSpeed)
	end

end

function controller:OnDeactivate()

	-- disconnect from the event bus when we spawn out.
	self.forwardBus:Disconnect()
	self.backwardBus:Disconnect()
	self.leftBus:Disconnect()
	self.rightBus:Disconnect()
	
	-- disconnect from mouse events
	self.mouseXBus:Disconnect()
	self.mouseYBus:Disconnect()

end

return controller
