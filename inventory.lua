local inventory = 
{
	Properties = 
	{
		PlayerEntity = {default = EntityId()}	,
		NumberOfKeys = {default = 1}	
	}
}

function inventory:OnActivate()

	-- We need to listen for key requests from gates. First, create an event ID (must match on sent from gate)
	-- Next, create the handler for the incoming events.
	self.keyRequestEventId = GameplayNotificationId(self.Properties.PlayerEntity, "KeyRequest")
	self.keyRequestHandler = GameplayNotificationBus.Connect(self, self.keyRequestEventId)
	

end

function inventory:OnEventBegin(value)

	-- Here we handle the incoming request for a key and send it back to the gate
	if(GameplayNotificationBus.GetCurrentBusId() == self.keyRequestEventId) then
	
		Debug.Log("[INVENTORY CONTROLLER] Received key request from gate: " .. tostring(value))
		
		-- Here is where we reduce the key inventory and send the confirmation back to the gate.
		-- First create the eventId (using the EntityId that is in the value
		-- Then send the notification.
		if(self.Properties.NumberOfKeys > 0) then
			Debug.Log("[INVENTORY CONTROLLER]  Sending opengate event to gate: " .. tostring(value))
			self.openGateEventId = GameplayNotificationId(value, "OpenGate")
			GameplayNotificationBus.Event.OnEventBegin(self.openGateEventId, "")
			-- substract one key
			self.Properties.NumberOfKeys = self.Properties.NumberOfKeys - 1
		else
			Debug.Log("[INVENTORY CONTROLLER] No keys in inventory.")	
		end
	end
end


function inventory:OnDeactivate()
	self.keyRequestHandler:Disconnect()
end

return inventory