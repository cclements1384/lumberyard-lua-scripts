local key_trigger = 
{
	Properties = 
	{
		playerEntityId = {default = EntityId()},
		numberOfKeysRequired = 1
	}
}

function key_trigger:OnActivate()

	self.triggerBus = TriggerAreaNotificationBus.Connect(self, self.entityId)

		-- create the event Id with the playerId and the "Key" name.
	self.keyActionId = GameplayNotificationId(self.Properties.playerEntityId,"Key")

end

function key_trigger:OnTriggerAreaEntered(enteredEntityId)
	Debug.Log("Entity entered key pickup area.")
	GameplayNotificationBus.Event.OnEventBegin(self.keyActionId, self.Properties.numberOfKeysRequired)
end

function key_trigger:OnTriggerAreaExited(exitedEntityId)
	Debug.Log("Enity exited key pickup area.")
end

function key_trigger:OnDeactivate()
	self.triggerBus:Disconnect()
	self.keyActionId = nil
end

return key_trigger