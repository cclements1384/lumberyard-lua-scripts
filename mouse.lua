local controller = 
{
	Properties = 
	{
		mouseX = "",
		mouseY = "",
		camSpeed = 0.0,
		mainCamera = { default = EntityId()}
	}
}

function controller:OnActivate()

	self.mouseXId = InputEventNotificationId(self.Properties.mouseX)
	self.mouseXBus = InputEventNotificationBus.Connect(self, self.mouseXId)
	
	self.mouseYId = InputEventNotificationId(self.Properties.mouseY)
	self.mouseYBus = InputEventNotificationBus.Connect(self, self.mouseYId)

end

function controller:OnHeld(InputValue)

	if(InputEventNotificationBus.GetCurrentBusId() == self.mouseXId) then
		TransformBus.Event.RotateByZ(self.entityId, - InputValue * self.Properties.camSpeed)
	end
	
	if(InputEventNotificationBus.GetCurrentBusId() == self.mouseYId) then
		TransformBus.Event.RotateByX(self.Properties.mainCamera, - InputValue * self.Properties.camSpeed)
	end
	

end 

function controller:OnDeactivate()

	self.mouseXBus:Disconnect()
	self.mouseYBus:Disconnect()

end

return controller