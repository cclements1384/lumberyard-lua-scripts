local movement = 
{
	Properties = 
	{
		MovementSpeed = .03 
	} 
}

function movement:OnActivate()

	self.forwardId = InputEventNotificationId("Forward")
	self.forwardBus = InputEventNotificationBus.Connect(self, self.forwardId)
	
	self.backwardId = InputEventNotificationId("Backward")
	self.backwardBus = InputEventNotificationBus.Connect(self, self.backwardId)

	self.leftId = InputEventNotificationId("Left")
	self.leftBus = InputEventNotificationBus.Connect(self, self.leftId)
	
	self.rightId = InputEventNotificationId("Right")
	self.rightBus = InputEventNotificationBus.Connect(self, self.rightId)

	
end

function movement:OnHeld(floatvalue)


	
	if(InputEventNotificationBus.GetCurrentBusId() == self.forwardId) then
		-- move the entity, while holding the forward button by the movement speed
		TransformBus.Event.MoveEntity(self.entityId, Vector3(0,self.Properties.MovementSpeed,0))
		end
	if(InputEventNotificationBus.GetCurrentBusId() == self.backwardId) then
		-- move the entity, while holding the backward button by the movement speed
		TransformBus.Event.MoveEntity(self.entityId, Vector3(0,-self.Properties.MovementSpeed,0))
	end
	
	if(InputEventNotificationBus.GetCurrentBusId() == self.leftId) then
		-- move the entity, while holding the left button by the movement speed
		TransformBus.Event.MoveEntity(self.entityId, Vector3(-self.Properties.MovementSpeed,0,0))
	end
	
		if(InputEventNotificationBus.GetCurrentBusId() == self.rightId) then
		-- move the entity, while holding the right button by the movement speed
		TransformBus.Event.MoveEntity(self.entityId, Vector3(self.Properties.MovementSpeed,0,0))
	end

end


function movement:OnDeactivate()

	-- disconnect from the event bus when we spawn out.
	self.forwardBus:Disconnect()
	self.backwardBus:Disconnect()
	self.leftBus:Disconnect()
	self.rightBus:Disconnect()

end

return movement
