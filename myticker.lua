local myticker = 
{
	Properties = 
	{
	}
}

function myticker:OnActivate()
	Debug.Log("Activated")
	
	self.tickBus = TickBus.Connect(self, 0)
	
	-- put a message on the gameplay notification bus
	self.gamePlayEventId = GamePlayNotificationBus.get
	
end

function myticker:OnTick(deltaTime, pointTime)

	Debug.Log("ticking:" .. tostring(deltaTime))

end

function myticker:OnDeactivate()
	Debug.Log("DeActivate")
	self.tickBus:Disconnect()
end

return myticker