local scriptcontroller1 = 
{
	Properties = 
	{
		TargetEntity = {default = EntityId()}
	}
}

function scriptcontroller1:OnActivate()
	self.actionNotificationId = GameplayNotificationId(self.Properties.TargetEntity, "Message")
	self.tickBus = TickBus.Connect(self)
end

function scriptcontroller1:OnTick(deltaTime, pointTime)
	GameplayNotificationBus.Event.OnEventBegin(self.actionNotificationId)
end

function scriptcontroller1:DeActivate()
 	self.tickBus:Disconnect()
end

return scriptcontroller1