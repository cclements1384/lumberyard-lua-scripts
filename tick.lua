local tick = 
{
	Properties = 
	{
	}
}

function tick:OnActivate()
	self.tickBus = TickBus.Connect(self, 0)  --priority -0 is the highest.
end

function tick:OnTick(deltaTime, timePoint)

	Debug.Log("tick...", timePoint)

end


function tick:OnDeactivate()

	self.tickBus:Disconnect()

end

return tick