local trigger = 
{
		Properties = 
		{
			     Player = {default = EntityId()}
		}

}

function trigger:OnActivate()

	self.TriggerArea = TriggerAreaNotificationBus.Connect(self, self.entityId)

end


function trigger:OnTriggerAreaEntered(EnteredEntity)

	Debug.Log("Something entered the trigger area.")
	-- check to see if it is our entity entering the trigger area
	if(EnteredEntity == self.Properties.Player) then
		Debug.Log("Player entered the area")
	end

end

function trigger:OnTriggerAreaExited(ExitedEntity)

	Debug.Log("Something exeited the trigger area")
	-- check to see if the exited entity is the player
	if(ExitedEntity == self.Properties.Player) then
		Debug.Log("Player exited the area.")
	end

end

function trigger:OnDeactivate()

	self.TriggerArea:Disconnect()

end

return trigger