local tutorial_scriptcontroller = 
{
	Properties = 
	{
	}
}

function tutorial_scriptcontroller:OnActivate()

	-- get the event id for the points
	self.PointsId = GameplayNotificationId(self.entityId, "Points") -- this needs to match the one that was sent.
	
	-- connect to the event bus and listen for events (using the Id we created above)
	self.PointsHandler = GameplayNotificationBus.Connect(self,self.PointsId)
	
	self.GamePlayHandler = GameplayNotificationBus.Connect(self)
	
end

function tutorial_scriptcontroller:OnEventBegin(value)

    Debug.Log("Received an event: " .. tostring(GameplayNotificationBus.GetCurrentBusId()))


	-- check to see if this event is for this Entity
	if(GameplayNotificationBus.GetCurrentBusId() == self.PointsId) then
	
		Debug.Log("Points received: " .. tostring(value))
	
	end

end

function tutorial_scriptcontroller:OnDeactivate()

	self.PointsHandler:Disconnect()
	seld.GamePlayHandler:Disconnect()
	

end

return tutorial_scriptcontroller

