local tutorial_trigger = 
{
	Properties = 
	{
		-- this entity contains the targeted Lua script
		TargetEntityId = {default = EntityId(), ...},
		PointValue = 10.0
		
	}
}

function tutorial_trigger:OnActivate()

	-- connect to the trigger bus
	self.trigger = TriggerAreaNotificationBus.Connect(self, self.entityId)
	
	-- get an action Id target EntityId + an action name
	self.gamePlayActionId = GameplayNotificationId(self.Properties.TargetEntityId, "Points")
	
	self.gateGamePlayActionId = GameplayNotificationId(self.entityId, "KeyCheck")
	
	

end

function tutorial_trigger:OnTriggerAreaEntered(enteredEntityId)

	-- log the entity that triggered the event.
	Debug.Log("[TRIGGER] Entity " ..  tostring(enteredEntityId) .. " entered.")
	
	-- send an event to the GameplayNotificationBus
	GameplayNotificationBus.Event.OnEventBegin(self.gamePlayActionId, self.Properties.PointValue)
	
	-- send a check to the key
	GameplayNotificationBus.Event.OnEventBegin(self.gateGamePlayActionId, 1) -- requires one key to open

end


function tutorial_trigger:OnDeactivate()

	-- disconnect from the trigger
	self.trigger:Disconnect()

end

return tutorial_trigger