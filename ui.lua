local loadUi =
{
			Properties = 
			{
			}

}

function loadUi:OnActivate()
   
  -- load the canvas.
	local canvasEnitityId = UiCanvasAssetRefBus.Event.LoadCanvas(self.entityId)
	
	-- show the mouse cursor (is this still needed?)
	LyShineLua.ShowMouseCursor(true)

	-- listen to events from the canvas
	self.canvasHandler = UiCanvasNotificationBus.Connect(self, canvasEnitityId)
	
			
end

function loadUi:OnAction(entityId, actionName)
   
    -- handle the action associated with a button click.
	if(actionName == "PlayClicked") then
	
		-- unload the canvas
		UiCanvasAssetRefBus.Event.UnloadCanvas(self.entityId)
		LyShineLua.ShowMouseCursor(false)
	
	end
	

end

function loadUi:OnDeactivate()

	self.canvasHandler:Disconnect()

end



return loadUi